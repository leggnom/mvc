const ObjectMachine = require('./object-machine');
const each = require('./helper/each');
const get_component = require('./helper/get-component');


/**
 * пример html
 * <div class="my-component" name="first-component">
 *     ...
 *
 *
 * пример js
 * import Component from 'mvc/src/component'
 *
 * class MyComponent extends Component {
 *      constructor(node) {
 *          super(node);
 *      }
 * }
 *
 * module.exports = Component.init('.my-component', MyComponent);
 *
 *
 * Параметр name в DOM необязательный, но при работе с событиями компоненты
 * на его основе можно определеить от какой из двух одинаковых компонент было возбуждено событие
 */
module.exports = class Component extends ObjectMachine {
    constructor(node) {
        super(node);
    }


    static init(query, instance, ...argv) {
        return function (parent, q=false) {
            query = q || query;

            each(parent.querySelectorAll(query), function (node) {
                if (!get_component(node, instance.name)) {
                    let args = [node].concat(argv);
                    new instance(...args);
                }
            });
        }
    }
};
