[назад](/doc/)

## ComponentCollection

#### ComponentCollection.add_list(list_components)
Добавить список компонент в коллекцию

Аргументы:
* list_components - спислк классов компонент

---

#### ComponentCollection.add(component)
Добавить компоненту в коллекцию

Аргументы:
* component - класс компоненты

---

#### ComponentCollection.get(component_name)
Получить класс компоненты по ее имени

Аргументы:
* component_name - название компоненты

---

#### ComponentCollection.prepare(parent_node, ...argv)
Найти в доме все компоненты из коллекции и создать их

Аргументы:
* parent_node - ссылка на DOM элемент внутри которого будет 
  происходить поиск компонент
* argv - аргументы передаваемые в конструктор компоненты

---

#### ComponentCollection.run(component_name, parent, options=[])
Создать компоненту

Аргументы:
* component_name - Название компоненты
* parent - ссылка на DOM элемент
* options - список параметров передаваемых в конструктор
