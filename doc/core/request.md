[назад](/doc/)
## Request

Объект request является обвязкой над методом 
[fetch](https://learn.javascript.ru/fetch). Необходимый для 
одинаковой отправки запросов как на сервере так и на клиенте.


#### Request.default_options
Дефолтные опции для отправки запросов
```javascript
{
    mode: 'cors',
    credentials: 'same-origin',
    headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
    }
}
```

---

#### Request.set_proxy(from, to)
Установка прокси
```javascript
Request.set_proxi('/api/', 'http://tactive.phoinix.dev.prototypes.ru/api/');

Request.get('/api/user/get');

// http://tactive.phoinix.dev.prototypes.ru/api/user/get
```
>>>
Важно подключать Request только после модуля `Server` или `Client`
```
// Правильно
const Server = require('mvc/server');
const Request = require('mvc/request');

// Вызовет ошибку
const Request = require('mvc/request');
const Server = require('mvc/server');
```
>>>

---

#### Request.get_path(path)
Получение пути с учетом установленных прокси

---

#### Request.prepare_response(response)
Обработка ответа от сервера 
* response = Объект Response 
Функция обязательно должна возврящать Promise

---

#### Request.merge_options(...options)
Объединение опций, возвращает новый объект

---

#### Request.attach(event_name, handler)
Подписаться на события объекта: `get`, `post`, `put`, `delete`

---

#### Request.get(path, options={})
Отправить запрос методом GET, если передан объект в options.body, 
он будет преобразован в query_string, во всех остальных случаях 
body будет проигнорирован 

---

#### Request.post(path, options={})
Отправить запрос методом POST

---

#### Request.put(path, options={})
Отправить запрос методом PUT

---

#### Request.delete(path, options={})
Отправить запрос методом DELETE

---

#### Request.dict_to_query(dict, prefix)
Преобразование объекта в query_string

---


Пример отправки запроса с данными
```javascript
const Request = require('mvc/request');
Request.get('/api/user/get', {
    body: {
        id: 1
    }
});
```


Пример отправки данных формы
```javascript
const Request = require('mvc/request');

let form = document.querySelector('form');  

Request.get('/api/user/get', {
    body: new FormData(form)
});
```
>>>
Важно, внутри контроллеров (и файлов которые используются на сервере) 
не использовать подобный метод отправки 
>>>