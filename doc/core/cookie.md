[назад](/doc/)

## Cookie

#### Cookie.replace(cookie_string)
Заменить все куки (вызывается в Client.js и Server.js при обновлении страници)

---

#### Cookie.set(key, value, options={})
Установить cookie на странице

---

#### Cookie.get(cookie_name)
Получить cookie по имени

---

#### Cookie.remove(cookie_name)
Удалить cookie

---

#### Cookie.toString()
Возвращяет строку со всеми cookie
