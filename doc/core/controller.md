[назад](/doc/)

## Controller

#### Controller.constructor(...requests)
* requests список реквестов (обязательно должны быть обернуты в Promise)

---

#### Controller.destructor()
Вызывается когда на странице изменяется список контроллеров 
и текущий будет уничтожен

---

#### Controller.on_reade()
Вызывется когда все реквесты отработали и страница была отрендерена, 
работает только на клиенте  

---

#### Controller.property(name, value)
Получение установка данных контекста контроллера аналог [App.property](./app.md)

---

#### Controller.render(template, context)
Задача на рендер страници, после выполнения всех реквестов 
будет вызванна функция [View.render](./view.md)

---

#### Controller.render_string(string)
Если задачи на рендер нет и не вызван `Controller.render`, на 
странице будет отрисованна переданная строка

---

#### Controller.get_template_content()
Запускает реквесты (если они переданны) и возвращает Promise  