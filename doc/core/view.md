[назад](/doc/)

## View

#### View.render(template, context={})
Рендер шаблона
template - ссылка на шаблон переданная через require()
context - объект с данными

`/views/head.html`
```html
<h1>{{ title }}</h1>
```


`main.js`
```javascript
const View = require('mvc/view');


let head = View.render(
    require('views/head.html'),
    {
        title: 'About'
    }
);

// head === '<h1>About</h1>'
```