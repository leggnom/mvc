[назад](/doc/)

## Helpers

#### refresh()
`helper/refresh`  
Перезагружает текущую страницу (работает как на клиенте так и на сервере)

---

#### redirect(path, code)
`helper/redirect`  
Перенаправление на другую страницу
* path - url адресс страници
* code - код запроса (по умолчанию 302) 

---

#### each(list, handler)
`helper/each`  
Аналог forEach, осуществляет перебор по масиву или NodeList

---

#### parse_json(string)
`helper/parse-json`  
Получение JSON из строки, если строка невалидная вернется пустой объект

---

#### parse_query(query_string)
`helper/parse-query`  
Получение объекта из строки запороса, если строка невалидная, то вернется пустой объект

---

#### clear_slashes(path)
`helper/clear-slashes`  
Удаление слушей в начале и конце строки

---

#### closest(node, selector)
`helper/closest`  
Поиск ноды по селектору от текущей ноды вверх, с перебором всех родителей

---

#### index_element(node)
`helper/index-element`  
Получение индекса элемента внутри родителя

---

#### create_component(node, instance)
`helper/create-component`  
Создать компонент

---

#### get_component(node, component_name=false)
`helper/get-component`  
Получить компоненту или список компонент

---

#### find_components(node)
`helper/find-component`  
Найти компоненты внутри родителя

---

#### parents_component(node)
`helper/parents-component`  
Получить всех предков компонент

---

#### is_object(value)
`helper/is-object`  
Проверка переменной на объект

---

#### is_string(value)
`helper/is-string`  
Проверка переменной на строку

---

#### is_array(value)
`helper/is-array`  
Проверка переменной на массив