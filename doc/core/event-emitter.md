[назад](/doc/)

## EventEmitter

#### EventEmitter.constructor()

---

#### EventEmitter.attach(event_name, handler)
Подписаться на событие 
* event_name - название события в виде строки
* handler - обработчик события

---

#### EventEmitter.dispatch(event_name, params=[])
Возбудить событие
* event_name - название события
* params - список параметров который будет передан в обработчик события


```javascript
const EventEmitter = require('mvc/event-emitter');

let myEvent = new EventEmitter();
 
myEvent.attach('custom_event', function(key) {
    console.log(index);
});

setTimeout(function(){
    myEvent.dispatch('custom_event', ['One']);
}, 100);
myEvent.dispatch('custom_event', ['Two']);


// -> Two
// -> One
```

---

#### remove(event_name, handler)
Удалить обработчик события

* event_name - название события в виде строки
* handler - обработчик события