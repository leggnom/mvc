[назад](/doc/)

## ObjectMachine
Осуществляет работу с компонентами и позволяет переключаться по дереву 
компонент вверх или низ

#### ObjectMachine.constructor(node)
Если `node` передан, будет создана компонента 

---

#### ObjectMachine.childrens()
Получить список всех потомков 

---

#### ObjectMachine.parents()
Получить список всех предков

---

#### ObjectMachine.parent(name)
Получить ближайшего предка по его имени

---

#### ObjectMachine.child(name)
Получить ближайшего потомка по его имени