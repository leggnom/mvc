[Документация](../)

* [Настройка окружения](./spa.md)
* Роуты
* [Наследование шаблонов, разделение сервера и клиента](spa-views.md)
* [Отправка запросов и обработка данных](spa-request.md)
* [Первая компонента](spa-component.md)
* [Сложные компоненты и работа с событиями](spa-event-component.md)
* [Добавление хелперов и работа с шаблонизатором](spe-template.md)

# Изоморфный SPA: роуты

Пользоваться функциями в роутах не очень удобно, когда 
количество страниц будет более 3-5, хочется более гибкий инструмент 
управления страницами. 
  
Для этой цели есть контроллеры, они отвечают не только за 
рендеринг страницы, но и за получение данных сервера, и за дальнейщее 
взаимодействие с DOM.
 
Для начала создадим `view` главной страницы (`source/views/home/home.html`):
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
    </head>
    <body>
        <h1>{{ header }}</h1>
    </body>
</html>
```
В дальнейшем `{{ header }}` заменится на переданное нами в 
контроллере значение 



Контроллер для главной страницы (`source/controller/home-controller.js`):
```javascript
const Controller = require('mvc/controller');

module.exports = class HomeController extends Controller {
    constructor() {
        super();

        this.render(require('../views/home/home.html'), {
            header: 'My first page'
        });
    }
};
```



Создадим файл со списком роутов (`source/route_list.js`), в 
дальнейшем нам это упрастит работу
```javascript
module.exports = {
    '/': require('./controller/home-controller')
};
``` 



И немного поправим `source/server.js`
```javascript
const Server = require('mvc/server');
const Route = require('mvc/route');

Route.add_list(require('./route_list'));

Server.run();
```



Теперь у нас есть список роутов `route_list.js`, в который мы можем 
добавлять новые роуты. Эти роуты будут использоваться не только для 
серверного рендеринга, но и для клиентского.