[Документация](../)

* [Настройка окружения](./spa.md)
* [Роуты](./spa-routes.md)
* [Наследование шаблонов, разделение сервера и клиента](./spa-views.md)
* Отправка запросов и обработка данных
* [Первая компонента](./spa-component.md)
* [Сложные компоненты и работа с событиями](./spa-event-component.md)
* [Добавление хелперов и работа с шаблонизатором](./spa-template.md)

# Изоморфный SPA: Отправка запросов и обработка данных

#### Отправка запросов

Для работы с АПИ используется компонент модуля Request, подключать его можно и нужно 
только в те контроллеры, в которых будет реализована работа с АПИ.

Давайте дополним `source/controller/home-controller.js`

```javascript
const BaseController = require('./base-controller');
const Request = require('mvc/request');

module.exports = class HomeController extends BaseController {
    constructor(params) {
        super(
            Request.get('http://tactive.phoinix.dev.prototypes.ru/api/red/lot/list', {
                body: params
            }).then(response => {
                return {
                    lots: response.success ? response.data.list : []
                }
            })
        );

        this.params = params;
        this.render(require('../views/home/home.html'), {
            header: 'List'
        });
    }
};
```
Как видно из примера мы подключили компонент `Request` и в конструкторе класса задали функцию
получения данных из АПИ. Результат ответа от АПИ доступен в переменной `lots`.

Метод `get` компонента `Request` принимает два параметра:
* путь до метода АПИ
* объект параметров запроса 

Теперь создадим шаблон для вывода данных, получаемых из АПИ. 
> Объявленная ранее в конструкторе контроллера переменная `lots` доступна в шаблонизаторе
и может использоваться в любом шаблоне

`source/views/home/home_lot-list.html`
``` html
{% for item in lots %}
    <li class="list__item">
        <a href="/lot/{{ item.id }}" class="list__link">{{ item.name }}</a>
    </li>
{% endfor %}
```
Подключим шаблон списка в `source/views/home/home.html`
```html
{% extends '../../component/layout/layout.html' %}

{% block content %}
    <h1>{{ header }}</h1>
    <ul class="list">
        {% include './home_lot-list.html' %}
    </ul>
{% endblock %}
```
#### Обработка запросов
На данном этапе у нас есть список ссылок на лоты. 

Добавим адрес на страницу лота в `source/route-list.js`
```javascript
module.exports = {
  '/': require('./controller/home-controller'),
  '/about': require('./controller/about-controller'),
  '/lot/<id>': require('./controller/lot-controller')
};
```
`/lot/<id>` - будет передаваться первым аргументом в конструктор контроллера в виде объекта
```javascript
{
    id: 'id'
}
```

Контроллер распарсит объект запроса, приведет его к строке и создаст адрес запроса к апи в формате

`API path + method path + request params`

В нашем примере будет

`http://tactive.phoinix.dev.prototypes.ru/api/red/lot/get?id=586e9508-0000-0000-0000-00007c7144ba`

Теперь можно добавить обработку запроса для получения информации о лоте.

Создадим контроллер `source/controller/lot-controller.js`
```javascript
const BaseController = require('./base-controller');
const Request = require('mvc/request');

module.exports = class Lot extends BaseController {
    constructor(params) {
        super(
            Request.get('http://tactive.phoinix.dev.prototypes.ru/api/red/lot/get', {
                body: params
            }).then(response => {
                return {
                    lot: response.success ? response.data.item : {}
                }
            })
        );

        this.params = params;
        this.render(require('../views/lot/lot.html'));
    }
};
```
Первым аргументом в конструктор класса передается параметр запроса

Сделаем шаблон для лота

`source/views/lot/lot.html`
```html
{% extends '../../component/layout/layout.html' %}

{% block content %}
<div class="lot">
    <h1 class="lot__ttl">{{ lot.name }}</h1>
    <div class="lot__info">
        <p class="lot__desc">{{ lot.description }}</p>
        <a href="{{ lot.external_trading }}" target="_blank" class="link">
            Внешний источник
        </a>
    </div>
</div>
{% endblock %}
```
Так же необходимо добавить роут для контроллера лота

`source/route_list.js`

#### Проксирование запросов
Проксирование запросов позволяет в контроллерах использовать относительные пути до методов
АПИ

Установим проксирование на сервере

`source/server.js`
```javascript
const Server = require('mvc/server');
const Request = require('mvc/request');
const View = require('mvc/view');
const Route = require('mvc/route');

Request.set_proxy('/api/', 'http://tactive.phoinix.dev.prototypes.ru/api/');

Route.add_list(require('./route_list'));

Server.prepare_page = function (content, context) {
    context['content'] = content;
    return View.render(require('./index.html'), context);
};

Server.run();
```
и на клиенте

`source/client.js`
```javascript
require('./main.scss');

const App = require('mvc/app');
const Client = require('mvc/client');
const Request = require('mvc/request');
const Route = require('mvc/route');
const constants = require('mvc/constants');

App.property('env', constants.ENV_ISOMORPHIC);
Route.add_list(require('./route_list'));

Request.set_proxy('/api/', 'http://tactive.phoinix.dev.prototypes.ru/api/');

Client.run();
```
>  Компонент Request должен подключатся в контроллер приложения после компонента 
Server или Client

Метод `set_proxy` принимает два параметра:
ключ (строка), по которому ищется соответствие в адресе; в данном примере ключ - это `/api/` (строка), 
заменяемая на значение переданное вторым аргументом, непосредственно url АПИ

Теперь поправим пути до методов АПИ в контроллерах

`source/controller/home-controller.js`
```javascript
const BaseController = require('./base-controller');
const Request = require('mvc/request');

module.exports = class HomeController extends BaseController {
    constructor(params) {
        super(
            Request.get('/api/red/lot/list', {
                body: params
            }).then(response => {
                return {
                    lots: response.success ? response.data.list : []
                }
            })
        );

        this.params = params;
        this.render(require('../views/home/home.html'), {
            header: 'List'
        });
    }
};
```
`controller/lot-controller.js`
```javascript
const BaseController = require('./base-controller');
const Request = require('mvc/request');

module.exports = class Lot extends BaseController {
    constructor(params) {
        super(
            Request.get('/api/red/lot/get', {
                body: params
            }).then(response => {
                return {
                    lot: response.success ? response.data.item : {}
                }
            })
        );

        this.params = params;
        this.render(require('../views/lot/lot.html'));
    }
};
```
