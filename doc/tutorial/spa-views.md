[Документация](../)

* [Настройка окружения](./spa.md)
* [Роуты](./spa-routes.md)
* Наследование шаблонов, разделение сервера и клиента
* [Отправка запросов и обработка данных](./spa-request.md)
* [Первая компонента](./spa-component.md)
* [Сложные компоненты и работа с событиями](./spa-event-component.md)
* [Добавление хелперов и работа с шаблонизатором](./spe-template.md)


# Изоморфный SPA: Наследование шаблонов, разделение сервера и клиента

Для более эффективной работы мы разделим шаблоны на слои, выделим 
отдельно мета данные и скрипты, отделим общие части страниц, шапку, навигацию, 
подвал, футер.


Для этого в nunjucks имеется ряд полезных функций, таких как `extend` и 
`block`.


#### Рендеринг на сервере != рендеринг на клиенте

Для начала выделим метаданные и скрипты которые нужно добавлять при 
рендеринге на сервере, но при рендеринге на клиенте будут мешать.
 
 
Создадим файл `source/index.html`
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>{{ title }}</title>
    </head>
    <body>
        {{ content | safe }}
    </body>
</html>
```


И немного поправим файл `source/server.js` переопределив дефолтную функцию модуля 
[Server.prepare_page](../core/server.md):
```javascript
const Server = require('mvc/server');
const View = require('mvc/view');
const Route = require('mvc/route');

Route.add_list(require('./route_list'));

Server.prepare_page = function (content, context) {
    context['content'] = content;
    return View.render(require('./index.html'), context);
};

Server.run();
```
Таким образом при рендеринге на сервере весь контент будет вставляться 
в страницу определенную в `source/index.html`


#### Разделение шаблонов на слои
После того как мета данные были выделены в отдельный файл, можно 
разделить шаблоны на слои, для начала определимся, что у нас будет на 
странице: шапка, подвал и левое меню. Все эти блоки должны 
перерисовываться при изменении урла.
   
Для начала создадим компоненты.

`source/component/header/header.html`
```html
<div class="header">
    <a href="/">SPA</a>
</div>
```


`source/component/menu/menu.html`
```jinja
<ul class="menu">
    {% for item in menu %}
        <li>
            <a href="{{ item.link }}">
                {{ item.name }}
            </a>
        </li>
    {% endfor %}
</ul>
```


`source/component/footer/footer.html`
```html
<div class="footer">
    <a href="/">SPA</a>
</div>
```


`source/component/layout/layout.html`
```jinja
<div class="layout">
    {% include '../header/header.html' %}
    <div class="grid">
        <div class="span-4">
            {% include '../menu/menu.html' %}
        </div>
        <div class="span-8">
            {% block content %}{% endblock %}
        </div>
    </div>
    {% include '../footer/footer.html' %}
</div>
```

Далее нужно немного поправить `views/home/home.html`
```jinja
{% extends '../../component/layout/layout.html' %}

{% block content %}
    <h1>{{ header }}</h1>
{% endblock %}
```


И в результате на странице у нас должен получится следующий вывод html`a:
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title></title>
    </head>
    <body>
        <div class="layout">
            <div class="header">
                <a href="/">SPA</a>
            </div>
            <div class="grid">
                <div class="span-4">
                    <ul class="menu">
    
                    </ul>
                </div>
                <div class="span-8">
                    <h1>My first page</h1>
                </div>
            </div>
            <div class="footer">
                <a href="/">SPA</a>
            </div>
        </div>
    </body>
</html>
```


Все замечательно, но у нас пустое меню, мы не передали в шаблоны 
переменную `menu`. Этот список будет участвовать при рендеринге на 
всех страницах (ну или почти на всех), каждый раз добавлять его внутри 
контроллера не очень удобно, поэтому мы сделаем дополнительный класс 
`BaseController`, от которого впредь и будем наследовать все наши 
контроллеры, а внутри него зададим общий контекст.

`source/controller/base-controller.js`
```javascript
const Controller = require('mvc/controller');


module.exports = class BaseController extends Controller {
    constructor(...requests) {
        super(...requests);
        this.property('menu', [
            {
                link: '/',
                name: 'Главная'
            },
            {
                link: '/about',
                name: 'О нас'
            }
        ]);
    }
};
```

И немного поправим `source/controller/home-controller.js` таким 
образом чтобы HomeController наследовался от BaseController:
```javascript
const BaseController = require('./base-controller');

module.exports = class HomeController extends BaseController {
    constructor() {
        super();

        this.render(require('../views/home/home.html'), {
            header: 'My first page'
        });
    }
};
```


Теперь у нас должно появится меню на странице всего с двумя пунктами, 
"Главная" и "О нас"
 
Добавим страницу about:
`source/controller/about-controller.js`
```javascript
const BaseController = require('./base-controller');

module.exports = class AboutController extends BaseController {
    constructor() {
        super();

        this.render(require('../views/about/about.html'), {
            header: 'О нас',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tristique pulvinar risus, non feugiat mi maximus eu. Sed sed ipsum eros. Nulla fermentum vitae lorem ut sodales. Mauris nec massa sit amet est rutrum rutrum a ut dui. Mauris dignissim erat vel arcu interdum imperdiet ornare vitae risus. Aliquam erat volutpat. Sed faucibus eros nisi, id bibendum diam interdum vitae. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi eu ligula odio. Vivamus congue dapibus felis, at scelerisque velit bibendum fermentum. Nam euismod porta velit, et aliquet massa euismod et. Quisque aliquet, magna sit amet aliquet hendrerit, sapien erat dignissim tellus, sollicitudin commodo libero nulla et dui. Proin dictum nulla odio, a varius nunc ullamcorper ac. Proin ullamcorper nibh eu malesuada volutpat. Morbi luctus at mi at dignissim.'
        });
    }
};
```

`source/views/about/about.html`
```jinja
{% extends '../../component/layout/layout.html' %}

{% block content %}
    <h1>{{ header }}</h1>
    {{ content }}
{% endblock %}
```

`source/route_list.js`
```javascript
module.exports = {
    '/': require('./controller/home-controller'),
    '/about': require('./controller/about-controller')
};
```

##### Страницы готовы, пора запустить наш клиент и добавить немного стилей
Добавим файл `source/client.js` с таким содержимым
```javascript
require('./main.scss');

const Client = require('mvc/client');
const Route = require('mvc/route');

Route.add_list(require('../route_list'));

Client.run();
```

Так же создадим несколько файлов в папке `source`:<br>
`main.scss`
```sass
@import "~glc/function";
@import "~glc/mixin";

@import "~glc/common/main";

@include grid();

@import "./component/component";
@import "./views/views";
```
`component/component.scss` - файл стилей компонентов

Так же необходимо создать папку `static` в корне проекта, в нее будут собираться файлы клиентских стилей 
и скриптов приложения 

Теперь подключим файл стилей `/static/main.css` и скрипт `/static/build.js` в `index.html`
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>{{ title }}</title>
        <link rel="stylesheet" href="/static/main.css"/>
    </head>
    <body>
        {{ content | safe }}
        <script src="/static/build.js"></script>
    </body>
</html>
```

Теперь можно собрать наше приложение и запустить сервер
```bash
npm run dev
npm start
```