const Cookie = require('../cookie');
const constants = require('../constants');
const parse_json = require('../helper/parse-json');
const parse_query = require('../helper/parse-query');


module.exports = class SystemRequest {
    constructor(request={}, body='') {
        let cookie = '';

        this.body_object = null;
        this._type = null;

        this._request = {
            url: request.url,
            method: request.method,
            referer: request.referer,
            body: body
        };

        if (request.headers) {
            this._request['headers'] = {
                cookie: request.headers.cookie,
                referer: request.headers.referer,
                'content-type': request.headers['content-type']
            };

            cookie = request.headers.cookie;
        }

        if (cookie) {
            Cookie.replace(cookie);
        }
    }


    get cookie() {
        return Cookie;
    }


    get url() {
        return this._request.url;
    }


    get referer() {
        return this._request.referer;
    }


    get method() {
        return this._request.method;
    }


    get body() {
        if (!this.body_object) {
            this.body_object = parse_json(this._request.body);

            if (!Object.keys(this.body_object)) {
                this.body_object = parse_query(this._request.body);
            }
        }

        return this.body_object;
    }


    get body_string() {
        return this._request.body
    }


    get headers() {
        return this._request.headers;
    }


    get type() {
        if (!this._type) {
            if (this._request.headers['content-type']) {
                this._type = this._request.headers['content-type'];
            } else {
                this._type = constants.REQUEST_CONTENT_TYPE_TEXT;
            }
        }

        return this._type;
    }
};