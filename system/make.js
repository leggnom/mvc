const App = require('../app');
const Route = require('../route');
const constants = require('../constants');
const SystemResponse = require('./system-response');
const each = require('../helper/each');
const parse_query = require('../helper/parse-query');


module.exports = function (url) {
    let query;
    let part;
    let promise_list = [];

    url = url.split('?');

    query = url[1];
    part = Route.check(url[0]);

    App.dispatch_event('page_destroy');


    /**
     * Вызываем деструкторы у текущего спсика контроллеров
     */
    each(App.current_controllers, cls => {
        if (cls.destructor) {
            cls.destructor();
        }
    });


    /**
     * Очищаем список ответов
     * @type {Array}
     */
    App.response = [];


    /**
     * Очищаем список текущих контроллеров
     * @type {Array}
     */
    App.current_controllers = [];


    if (part.route) {
        part.route.handler.map(handler => {

            if (handler.prototype && handler.prototype.promise) {
                let cls = new handler(part.params, parse_query(query));

                App.current_controllers.push(cls);
                promise_list.push(cls.promise());

            } else {
                promise_list.push(new Promise(handler).then(response => {
                    new SystemResponse(response, 200);
                }));
            }
        });
    }

    return Promise.all(promise_list).then(() => {
        return response_join(App.response);
    });
};


function response_join(response_list) {
    let code = constants.REQUEST_CODE_NOT_FOUND;
    let body = {
        html: [],
        json: []
    };

    let headers = {};

    each(response_list, item => {
        code = item.code;
        headers = item.headers;
        if (typeof item.body === 'string') {
            body['html'].push(item.body);
        } else if (typeof item.body === 'object') {
            body['json'].push(item.body);
        }
    });

    return new SystemResponse(body, code, headers);
}