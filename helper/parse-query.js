module.exports = function parse_query(query_string) {
    if (typeof query_string != "string" || query_string.length == 0) {
        return {};
    }

    query_string = (query_string[0] === '?') ? query_string.substr(1) : query_string;
    query_string = query_string.split("&");

    let s_length = query_string.length;
    let bit;
    let query = {};
    let first;
    let second;

    for(var i = 0; i < s_length; i++) {
        bit = query_string[i].split("=");
        first = decodeURIComponent(bit[0]);

        if (first.length == 0) {
            continue;
        }

        second = decodeURIComponent(bit[1]);

        if (typeof query[first] == "undefined") {
            query[first] = second;
        } else if (query[first] instanceof Array) {
            query[first].push(second);
        } else {
            query[first] = [query[first], second];
        }
    }

    return query;
};