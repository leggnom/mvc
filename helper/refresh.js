const App = require('../app');
const constants = require('../constants');
const redirect = require('./redirect');


module.exports = function refresh() {
    if (App.property('space') === constants.SPACE_CLIENT) {
        return App.dispatch_event('refresh', [App.response.referer]);
    }

    redirect(App.response.referer);
};