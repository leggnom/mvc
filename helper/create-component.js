/**
 * Создать компонент
 */
module.exports = function create_component(node, instance) {
    if (!node.__components__) {
        node.classList.add('component');
        node.__components__ = [];
    }

    node.__components__.push(instance);
    return instance;
};