const App = require('../app');
const constants = require('../constants');
const SystemResponse = require('../system/system-response');
const Cookie = require('../cookie');


module.exports = function redirect(path, code) {
    let cookie = Cookie.toString();
    let headers = {
        'Location': path
    };

    if (App.property('space') === constants.SPACE_CLIENT) {
        return App.dispatch_event('redirect', [path]);
    }

    if (cookie) {
        headers['Set-Cookie'] = cookie;
    }

    new SystemResponse('', code || 302, headers);
};