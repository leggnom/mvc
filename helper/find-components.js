const each = require('./each');

/**
 * Найти компоненты внутри родителя
 * @param node
 * @returns {Array}
 */
module.exports = function find_components(node) {
    let result = [];

    each(node.querySelectorAll('.component'), node => {
        result.push(node.__components__ || []);
    });

    return result;
};