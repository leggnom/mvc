/**
 * Удаление слушей в начале и конце строки
 * @param path
 * @returns {string}
 */
module.exports = function clear_slashes(path) {
    return '' + path.toString().replace(/\/$/, '').replace(/^\//, '');
};