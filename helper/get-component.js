const each = require('./each');

/**
 * Получить компонент или список компонент
 * @param node
 * @param component_name
 */
module.exports = function get_component(node, component_name=false) {
    let component = null;
    let component_list = node.__components__ || [];

    each(component_list, item => {
        if (item.constructor.name === component_name) {
            component = item;
            return false;
        }
    });

    return component;
};