module.exports = function is_object(value) {
    return value ? Object.prototype.toString.call(value) === '[object Object]' : false;
};
