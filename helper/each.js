/**
 * Обертка над forEach
 * @param list
 * @param handler
 */
module.exports = function each(list, handler) {
    if (!list) {
        return;
    }

    if (!Array.isArray(list)) {
        list = Array.prototype.slice.call(list);
    }

    for (let i = 0; i < list.length; i++) {
        if (handler(list[i], i) === false) {
            break;
        }
    }
};