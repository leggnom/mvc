const App = require('./app');
const each = require('./helper/each');


module.exports = {
    _list: {},

    _push(key, value='', options={}) {
        key = key.trim();
        value = value.trim();
        let string = [key + '=' + value];

        if (!options['path']) {
            options['path'] = '/'
        }

        each(Object.keys(options), key => {
            let value = options[key];

            if (key === 'expires') {
                let date;
                if (isNaN(value)) {
                    date = new Date(value);
                } else {
                    date = new Date(value);
                    date = date.setDate(date.getDate() + value);
                }
                value = date.toUTCString();
            }

            string.push(key + '=' + value);
        });

        return this._list[key] = {
            key: key,
            value: value,
            options: options,
            string: string.join(';')
        };
    },

    replace(cookie_string='') {
        this._list = {};

        if (!cookie_string) {
            return;
        }

        each(cookie_string.split(' '), item => {
            let part = item.split('=');
            this._push(part[0], part[1]);
        });
    },

    set(key, value, options={}) {
        if (key) {
            App.dispatch_event('set_cookie', [
                this._push(key, value, options)
            ]);
        }
    },

    get(cookie_name) {
        return this._list.hasOwnProperty(cookie_name) ? this._list[cookie_name].value : false;
    },

    remove(cookie_name) {
        App.dispatch_event('remove_cookie', [
            this._push(cookie_name, '', {
                expires: 'Thu, 01 Jan 1970 00:00:01 GMT'
            })
        ]);
    },

    toString() {
        let string = [];

        each(Object.keys(this._list), key => {
            string.push(this._list[key].string);
        });

        return string.join('; ');
    }
};