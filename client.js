const App = require('./app');
const Cookie = require('./cookie');
const make = require('./system/make');
const closest = require('./helper/closest');
const each = require('./helper/each');
const constants = require('./constants');
const SystemRequest = require('./system/system-request');


App.attach_event('remove_cookie', cookie => {
    set_cookie(cookie.key, cookie.value, cookie.options);
});


App.attach_event('set_cookie', cookie => {
    set_cookie(cookie.key, cookie.value, cookie.options);
});


App.attach_event('redirect', path => {
    window.location = path;
});


App.attach_event('refresh', () => {
    window.location.reload();
});


App.property('space', constants.SPACE_CLIENT);


module.exports = {

    /**
     * Старт приложения
     */
    run() {
        Cookie.replace(document.cookie);

        App.property('path', window.location.pathname + window.location.search);
        this.check(window.location.pathname + window.location.search);
        this.listen();
    },


    check(path) {
        App.clear_context();
        App.property('path', path);

        App.request = new SystemRequest();

        make(path).then(response => {
            if (response.code === constants.REQUEST_CODE_SUCCESS) {
                if (App.property('env') === constants.ENV_CLIENT) {
                    document.body.innerHTML = response.get_content(constants.REQUEST_CONTENT_TYPE_HTML);
                }

                if (App.property('env') === constants.ENV_ISOMORPHIC) {
                    App.property('env', constants.ENV_CLIENT);
                }

                App.dispatch_event('page_ready', [response]);

                App.component.prepare();
            }
        }).catch(err => {
            App.dispatch_event('page_error', [err]);
        });
    },


    listen() {
        let pushState = history.pushState;

        history.pushState = function(state) {
            let ps = pushState.apply(history, arguments);
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            return ps;
        };

        document.removeEventListener('click', history_target);
        document.addEventListener('click', history_target);

        window.onpopstate = history.onpushstate = function () {
            let path = window.location.pathname + window.location.search;
            let hash = window.location.hash;

            if (hash) {
                App.dispatch_event('change_hash', [hash]);
                return;
            }

            if(path !== App.property('path')) {
                App.dispatch_event('change_url', [path]);
                this.check(path);
            }
        }.bind(this);
    }
};


/**
 * Обработка клика по ссылкам
 * @param event
 */
function history_target(event) {
    let a = closest(event.target, 'a');

    if (!a) {
        return;
    }

    if (window.location.host !== a.host) {
        return;
    }

    if (App.property('env') !== constants.ENV_CLIENT) {
        return;
    }

    event.preventDefault();
    history.pushState(null, null, a.getAttribute('href'));
}


/**
 * Установка Cookie
 * @param name
 * @param value
 * @param options
 */
function set_cookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}