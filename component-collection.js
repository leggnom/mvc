const each = require('./helper/each');
const parse_json = require('./helper/parse-json');


module.exports = {
    _collection: [],


    /**
     * Добавить список компонент
     * @param list_components
     */
    add_list(list_components) {
        each(list_components, component => {
            this.add(component);
        });
    },


    /**
     * Добавить компоненту
     * @param component
     */
    add(component) {
        this._collection.push(component);
    },


    /**
     * Получить компоненту
     * @param component_name
     * @returns {boolean}
     */
    get(component_name) {
        let component = false;

        each(this._collection, item => {
           if (item.name === component_name) {
               component = item;
               return false;
           }
        });

        return component;
    },


    /**
     * Найти в DOM все компоненты
     * @param parent_node
     * @param argv
     */
    prepare(parent_node, ...argv) {
        var parent_node = parent_node || document;

        each(this._collection, item => {
            if (item.query) {
                each(parent_node.querySelectorAll(item.query()), parent => {
                    this.run(item.name, parent, argv);
                });
            }
        });


        each(parent_node.querySelectorAll('script'), item => {
            if (item.hasAttribute('data-component') && !item.getAttribute('data-run')) {
                let name = item.getAttribute('data-component');
                let parent = item.parentNode;
                let options = item.innerHTML;

                options = options.replace(/&gt;/g, '>');
                options = options.replace(/&lt;/g, '<');
                options = parse_json(options) || [];

                item.setAttribute('data-run', 1);
                this.run(name, parent, options);
            }
        });
    },


    /**
     * Запустить компоненту
     * @param component_name
     * @param parent
     * @param options
     */
    run(component_name, parent, options=[]) {
        let component = this.get(component_name);
        if (component) {
            new component(parent, ...options);
        }
    }
};