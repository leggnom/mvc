const each = require('./helper/each');
const clear_slashes = require('./helper/clear-slashes');


const URL_PARAM_REGEXP = new RegExp('<(.*)>', 'i');


module.exports = {
    /**
     * Обязательный последний слеш
     */
    is_last_slashes: true,


    /**
     * Список роутов
     */
    route_list: {},


    /**
     * Проверка на существование url в списке
     * @param url
     * @returns {{route: boolean, params: boolean}}
     */
    check(url) {
        let self_parts = this.path_to_parts(url);
        let route = false;
        let route_params = false;

        each(Object.keys(this.route_list), item => {
            let params = this._get_parts_params(this.route_list[item], self_parts);

            if (params !== false) {
                route = this.route_list[item];
                route_params = params;
                return false;
            }
        });

        return {
            route: route,
            params: route_params,
        }
    },


    /**
     * Добавление роута в список
     * @param route
     * @param handler
     */
    add(route, handler) {
        if (this.route_list[route]) {
            this.route_list[route]['handler'].push(handler);
        } else {
            this.route_list[route] = {
                path: route,
                parts: this.path_to_parts(route),
                handler: [handler]
            }
        }
    },


    /**
     * Добавить список роутов
     * @param route_list
     */
    add_list(route_list) {
        each(Object.keys(route_list), route => {
            this.add(route, route_list[route]);
        });
    },


    /**
     * Сравнение роутов
     * @param route
     * @param parts
     * @returns {boolean}
     */
    _get_parts_params(route, parts) {
        let params = false;

        if (route.parts.length === parts.length) {
            params = {};

            each(route.parts, (route_part, index) => {
                if (route_part.variable) {
                    params[route_part.variable] = parts[index].value;

                } else {
                    if (route_part.value !== parts[index].value) {
                        return params = false;
                    }
                }

            });
        }

        return params;
    },


    /**
     * Преобразовать урл в объект
     * @param path
     * @returns {Array}
     */
    path_to_parts(path) {
        let parts = [];
        let new_path = this.check_slashes(path);

        each(new_path.split('/'), item => {
            let part = {
                value: item,
                variable: ''
            };

            if (item.match(URL_PARAM_REGEXP)) {
                part['variable'] = item.replace(/[<>\n]/g, '');
            }

            parts.push(part);
        });

        return parts;
    },


    /**
     * Уберает лишние слешы из пути
     * @param path
     * @returns {string}
     */
    check_slashes(path) {
        let last_slash = '';
        if (this.is_last_slashes && path.substr(-1) == '/') {
            last_slash = '/';
        }

        return clear_slashes(path) + last_slash;
    }
};