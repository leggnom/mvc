const App = require('./app');
const Route = require('./route');
const each = require('./helper/each');
const constants = require('./constants');
const parse_query = require('./helper/parse-query');


module.exports = {
    handlers: {},

    add_handler(handler) {
        if (handler.name) {
            this.handlers[handler.name] = handler;
        }
    },


    /**
     * Обработка урла, загрузка реквестов
     * @param url
     * @returns {Promise.<TResult>}
     */
    check(url) {
        url = url.split('?');
        let query = url[1];
        let part = Route.check(url[0]);
        let code = constants.REQUEST_CODE_NOT_FOUND;
        let promise_list = [];

        App.dispatch_event('page_destroy');

        each(App.current_controllers, cls => {
            if (cls.destructor) {
                cls.destructor();
            }
        });

        delete App.current_controllers;
        App.current_controllers = [];

        if (part.route) {
            part.route.handler.map(handler => {
                if (handler.prototype && handler.prototype.get_template_content) {
                    let cls = new handler(part.params, parse_query(query));
                    App.current_controllers.push(cls);

                    promise_list.push(
                        cls.get_template_content()
                    );

                } else {
                    promise_list.push(
                        new Promise(handler)
                    );
                }
            });
            code = constants.REQUEST_CODE_SUCCESS;
        }

        return Promise.all(promise_list).then(content => {
            if (Array.isArray(content)) {
                return content.join('');
            }
        }).then(content => {
            return {
                headers: {
                    code: code
                },
                content: content
            }
        });
    }
};
