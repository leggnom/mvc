const EventMachine = require('./event-machine');
const each = require('./helper/each');
const create_component = require('./helper/create-component');
const parents_component = require('./helper/parents-component');
const find_components = require('./helper/find-components');


module.exports = class ObjectMachine extends EventMachine {
    constructor(node) {
        super();

        if (node) {
            this.node = node;
            this.name = node.getAttribute('name');
            create_component(node, this);
        }
    }

    /**
     * Получить все потомков
     * @returns {*}
     */
    get childrens() {
        if (this.node) {
            this._child_components = find_components(this.node);
        }
        return this._child_components;
    }


    /**
     * Получить всех предков
     * @returns {*}
     */
    get parents() {
        if (this.node) {
            this._parent_components = parents_component(this.node);
        }
        return this._parent_components;
    }


    /**
     * Получить ближайшего предка по его имени
     * @param name
     * @returns {*}
     */
    parent(name) {
        return get_component(this.parents, name);
    }


    /**
     * Получить ближайшего потомка по его имени
     * @param name
     * @returns {*}
     */
    child(name) {
        return get_component(this.childrens, name);
    }
};


//TODO сделать асинхронным
function get_component(env, name) {
    let result_component = false;

    if (!env || !env.length) {
        return result_component;
    }

    if (!name) {
        return env[0];
    }

    each(env, parent => {
        if (result_component) {
            return false;
        }

        each(parent, component => {
            if (component.constructor.name == name) {
                result_component = component;
                return false;
            }
        });
    });

    return result_component;
};
