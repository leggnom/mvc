const App = require('./app');
const Cookie = require('./cookie');
const EventEmitter = require('./event-emitter');
const constants = require('./constants');
const each = require('./helper/each');
const is_object = require('./helper/is-object');
const is_string = require('./helper/is-string');


const RequestEvents = new EventEmitter();


module.exports = {
    _proxy_list: [],


    default_options: {
        mode: 'cors',
        credentials: 'same-origin',
        headers: {
            "cookie": Cookie.toString()
        }
    },


    set_proxy(from, to) {
        this._proxy_list.push({
            from: from,
            to: to
        });
    },


    get_path(path) {
        let result = path
        each(this._proxy_list, item => {
            if (path.indexOf(item.from) === 0) {
                result = item.to + path.replace(item.from, '');
                return 0;
            }
        });
        return result;
    },


    prepare_response(response) {
        let type = response.headers.get("content-type");

        if (type.includes('json')) {
            return response.json();
        } else {
            return response.text();
        }
    },


    merge_options(...options) {
        let cookie = Cookie.toString();

        if (cookie) {
            options.unshift({
                headers: Object.assign(this.default_options.headers, {
                    "cookie": Cookie.toString()
                })
            });
        }
        options = [{}, this.default_options].concat(...options);
        return Object.assign(...options);
    },


    get(path, options={}) {
        return this._make(path, 'get', options);
    },


    post(path, options={}) {
        return this._make(path, 'post', options);
    },


    put(path, options={}) {
        return this._make(path, 'put', options);
    },


    delete(path, options={}) {
        return this._make(path, 'delete', options);
    },


    attach: RequestEvents.attach,


    /**
     * Преобразование объекта в query_string
     * @param dict
     * @param prefix
     * @returns {string}
     */
    dict_to_query(dict, prefix) {
        let item;
        let query = [];
        for(item in dict) {
            if (dict.hasOwnProperty(item)) {
                let prefix_key = isNaN(item) ? item : '';
                let key = prefix ? prefix + "[" + prefix_key + "]" : item;
                let value = dict[item];
                let chunk;

                if (value !== null && typeof value === "object") {
                    chunk = this.dict_to_query(value, key);
                } else {
                    chunk = encodeURIComponent(key) + "=" + encodeURIComponent(value);
                }

                query.push(chunk);
            }
        }
        return query.join("&");
    },


    _merge_body(body) {
        if (this.default_options.body) {
            if (is_object(this.default_options.body) && is_object(body)) {
                body = Object.assign({}, this.default_options.body, body);
            }

            if (is_string(this.default_options.body) && is_string(body)) {
                body = [this.default_options.body, body].join('\n');
            }
        }

        return body;
    },


    _make(path, method, options) {
        let env = App.property('env');
        let space = App.property('space');

        if (space === constants.SPACE_CLIENT) {
            if ([constants.ENV_SERVER, constants.ENV_ISOMORPHIC].includes(env)) {
                return new Promise((resolve, reject) => {
                    resolve({});
                });
            }
        }

        return new Promise((resolve, reject) => {
            options.body = this._merge_body(options.body);

            if (method === 'get') {
                if (is_object(options.body)) {
                    path = path + '?' + this.dict_to_query(options.body);
                }
                delete options.body;
            } else {
                if (is_object(options.body)) {
                    options.body = this.dict_to_query(options.body);
                }
            }


            fetch(this.get_path(path), this.merge_options({method: method}, options)).then(response => {
                RequestEvents.dispatch(method, [response]);
                resolve(this.prepare_response(response))
            }).catch(err => {
                reject(err);
            });

        });
    }
};
