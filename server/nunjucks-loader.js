const nunjucks = require('nunjucks');
const env = new nunjucks.Environment(new nunjucks.FileSystemLoader);

nunjucks.configure('/', {
    autoescape: true,
});


require.extensions['.html'] = function (module, filename) {
    module.exports = {
        render: function (context) {
            return env.render(filename, context);
        }
    }
};


require.extensions['.svg'] = function (module, filename) {
    module.exports = fs.readFileSync(module, 'utf8');
};


module.exports = env;