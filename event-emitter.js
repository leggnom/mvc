const each = require('./helper/each');


module.exports = class EventEmitter {
    constructor() {
        this._events = {};
    }

    /**
     * Подписаться на событие
     * @param event_name
     * @param handler
     */
    attach(event_name, handler) {
        if (!this._events.hasOwnProperty(event_name)) {
            this._events[event_name] = [];
        }

        this._events[event_name].push(handler);
    }


    /**
     * Возбудить событие
     * @param event_name
     * @param params
     */
    dispatch(event_name, params=[]) {
        if (this._events.hasOwnProperty(event_name)) {
            this._events[event_name].forEach(function (handler) {
                handler.apply({}, params);
            });
        }
    }


    /**
     * Удаление события
     * @param event_name
     * @param handler
     */
    remove(event_name, handler) {
        let list = [];

        if (this._events.hasOwnProperty(event_name)) {
            each(this._events[event_name], (item, index) => {
                if (item.constructor !== handler.constructor) {
                    list.push(item);
                }
            });

            this._events[event_name] = list;
        }
    }
};
