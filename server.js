const http = require('http');
const url = require('url');

const write_file = require('./server/write-file');
const App = require('./app');
const Cookie = require('./cookie');
const SystemRequest = require('./system/system-request');
const constants = require('./constants');
const env = require('./server/nunjucks-loader');
const make = require('./system/make');

let server;


App.property('env', constants.ENV_SERVER);


App.property('space', constants.SPACE_SERVER)


global.fetch = require('node-fetch');


module.exports = {

    static_path: '/static/',


    /**
     * Предворительная обработка страниц
     * @param content
     * @param context
     * @returns {*}
     */
    prepare_page(content, context, response) {
        return content;
    },


    /**
     * Запуск сервера
     * @param port
     * @param host
     */
    run(port=5000, host='127.0.0.1') {
        this.get_server().listen(port, host);
    },


    /**
     * Получение ссылки и старт сервера
     * @returns {*}
     */
    get_server() {
        if (server) {
            return server;
        }

        return server = http.createServer((req, res) => {
            let path = url.parse(req.url).path;

            if (path.includes(this.static_path) || path.includes('.ico')) {
                write_file(req, res);

            } else {
                var body = [];

                App.response = [];

                req.on('error', function(err) {
                    console.log(err)

                }).on('data', function(chunk) {
                    body.push(chunk);

                }).on('end', () => {
                    let request;

                    body = Buffer.concat(body).toString();

                    App.request = request = new SystemRequest(req, decodeURIComponent(body));

                    App.clear_context();
                    App.property('path', path);

                    make(path).then(response => {
                        if (response.code < 400) {
                            let cookie = Cookie.toString();
                            let content = '';
                            let headers = {
                                'Content-Type': 'text/html',
                                'Accept-Ranges': 'bytes',
                                'Cache-Control': 'no-cache'
                            };

                            if (cookie) {
                                headers['Set-Cookie'] = cookie;
                            }

                            res.writeHead(response.code, Object.assign(headers, response.headers));

                            res.end(
                                this.prepare_page(
                                    response.get_content(request.type),
                                    App.get_context(),
                                    response
                                )
                            );
                        }
                    }).catch(err => {
                        res.end('' + err);
                        console.log(err);
                    });
                });
            }
        });
    }
};
