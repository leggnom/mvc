const App = require('./app');
const View = require('./view');
const EventEmitter = require('./event-emitter');
const SystemResponse = require('./system/system-response');
const constants = require('./constants');
const wait = require('./system/wait');


module.exports = class Controller extends EventEmitter {
    constructor(...requests) {
        let event_halnder = () => {
            this.on_ready();
        };

        super();

        App.remove_event('page_ready', event_halnder);
        App.attach_event('page_ready', event_halnder);


        /**
         * Данные контролера для рендеринга
         * @type {{}}
         */
        this.scope = Object.assign({}, App._scope);


        this._response = null;


        /**
         * Отправка запросов и их ожидание
         */
        this._wait = wait(...requests);
    }


    destructor() {

    }


    on_ready() {

    }


    /**
     * Получение установка данных контекста контроллера
     * @param name
     * @param value
     * @returns {*}
     */
    property(name, value) {
        return this.scope[name] = value !== undefined ? value : this.scope[name];
    }


    /**
     * Отрендерить шаблон
     * @param template
     * @param context
     */
    render(template, context={}) {
        this._wait.then(response => {
            let content;
            response.unshift(this.scope);
            response.push(context);

            Object.assign(...response);

            App.set_context(this.scope);

            if (App.property('space') === constants.SPACE_SERVER) {
                content = View.render(template, this.scope);
            } else {
                if (App.property('env') === constants.ENV_CLIENT) {
                    content = View.render(template, this.scope);
                }
            }

            this._response = new SystemResponse(content);
        });
    }


    /**
     * Вернуть строку
     * @param string
     */
    render_string(string) {
        this._response = new SystemResponse(string);
    }


    /**
     * Сформировать свйот ответ от сервера
     * @param body
     * @param code
     * @param headers
     */
    response(body, code, headers) {
        this._response = new SystemResponse(body, code, headers);
    }


    /**
     * Добавить задачу в список ожидания
     * @param requests
     */
    wait(...requests) {
        this._wait.add(...requests);
    }


    /**
     * Подписаться на выполнения всех задач
     * @param handler
     */
    then(handler) {
        this._wait.then(handler);
    }


    /**
     * Подписать на ошибку выполнения задач
     * @param handler
     */
    catch(handler) {
        this._wait.catch(handler);
    }


    promise() {
        return this._wait._run();
    }
};